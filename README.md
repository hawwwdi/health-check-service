# health-check-service
## API
### create api
`POST /api/create`  
body:  
```json
{
	"url" : "http://localhost:3002/api/1",
	"method": "GET",
	"interval": 15,
	"body": "something",
	"headers": {
		"key1":"value1"
	}
}
```
response:   
`200 OK`
```json
{
  "id": 1
}
```
- - - - 
### delete api
`POST /api/delete`  
body:
```json
{
  "id": 1
}
```
response:  
`200 OK`
- - - -
### start api
`POST /api/start`  
body:
```json
{
  "id": 1
}
```
response:  
`200 OK`
- - - -
### stop api
`POST /api/stop`  
body:
```json
{
  "id": 1
}
```
response:  
`200 OK`
- - - -
### get all api
`GET /`  
response:  
`200 OK`
body:  
```json
[
	{
		"id": 2,
		"url": "localhost:3001",
		"method": "POST",
		"body": "abc",
		"headers": {
			"key1": "value1"
		},
		"interval": 5,
		"started": true
	}
]
```
