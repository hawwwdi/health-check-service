package apiservice

import (
	"context"
	"fmt"
	"log"

	"github.com/hawwwdi/health-check-service/models"
)

var noContext = context.Background()

type apiService struct {
	store      ApiStore
	cache      Cache
	scheduler  Scheduler
	webhook    Webhook
	resultChan chan models.APIResponse
}

func NewApiService(apiStore ApiStore, scheduler Scheduler, webhook Webhook, cache Cache, bufferSize int) *apiService {
	return &apiService{
		store:      apiStore,
		cache:      cache,
		scheduler:  scheduler,
		webhook:    webhook,
		resultChan: make(chan models.APIResponse, bufferSize),
	}
}

func (a *apiService) Start(numOfWorkers int) error {
	log.Println("starting...")
	apis, err := a.store.GetAll(noContext)
	if err != nil {
		return err
	}
	for _, api := range apis {
		if !api.Started {
			continue
		}
		err = a.scheduler.ScheduleJob(api.AsJob(), a.resultChan)
		if err != nil {
			return err
		}
	}
	for i := 0; i < numOfWorkers; i++ {
		log.Printf("start worker %d\n", i+1)
		go worker(a.resultChan, a.store, a.cache, a.webhook)
	}
	a.scheduler.Start()
	return nil
}

func (a *apiService) CreateApi(api models.API) (int, error) {
	return a.store.Create(noContext, api)
}

func (a *apiService) DeleteApi(id int) error {
	err := a.store.Delete(noContext, id)
	if err != nil {
		return err
	}
	_ = a.scheduler.StopJob(fmt.Sprintf("%d", id))
	return nil
}

func (a *apiService) StartApi(id int) error {
	err := a.store.Start(noContext, id)
	if err != nil {
		return err
	}
	api, err := a.store.Get(noContext, id)
	if err != nil {
		return err
	}
	err = a.scheduler.ScheduleJob(api.AsJob(), a.resultChan)
	if err != nil {
		return err
	}
	return nil
}

func (a *apiService) StopApi(id int) error {
	err := a.store.Stop(noContext, id)
	if err != nil {
		return err
	}
	err = a.scheduler.StopJob(fmt.Sprintf("%d", id))
	if err != nil {
		return err
	}
	return nil
}

func (a *apiService) GetAllApi() ([]models.API, error) {
	return a.store.GetAll(noContext)
}
