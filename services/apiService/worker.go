package apiservice

import (
	"fmt"
	"log"

	"github.com/hawwwdi/health-check-service/models"
	"github.com/hawwwdi/health-check-service/store/cache"
)

func worker(resultChan chan models.APIResponse, db ApiStore, cacheStore Cache, webhook Webhook) {
	for res := range resultChan {
		var lastResponse models.APIResponse
		lastHealth, err := cacheStore.Get(fmt.Sprintf("%d", res.Id))
		if err == cache.ErrKeyNotFound {
			lastResponse, err = db.GetLastResponse(noContext, res.Id)
			lastHealth = fmt.Sprintf("%v", lastResponse.Healthy)
		}
		if lastHealth != fmt.Sprintf("%v", res.Healthy) && err == nil {
			lastResponse, _ = db.GetLastResponse(noContext, res.Id)
			req := models.WebhookRequest{
				New:  res,
				Last: lastResponse,
			}
			err = webhook.Invoke(req)
			if err != nil {
				log.Println(err)
			}
		}
		err = cacheStore.Set(fmt.Sprintf("%d", res.Id), fmt.Sprintf("%v", res.Healthy))
		if err != nil {
			log.Println(err)
		}
		err = db.SubmitResponse(noContext, res)
		if err != nil {
			log.Println(err)
		}
	}
}
