package apiservice

import (
	"context"

	"github.com/hawwwdi/health-check-service/models"
)

type ApiStore interface {
	Create(ctx context.Context, api models.API) (int, error)
	Delete(ctx context.Context, id int) error
	Start(ctx context.Context, id int) error
	Stop(ctx context.Context, id int) error
	GetAll(ctx context.Context) ([]models.API, error)
	Get(ctx context.Context, id int) (models.API, error)
	SubmitResponse(ctx context.Context, response models.APIResponse) error
	GetLastResponse(ctx context.Context, id int) (models.APIResponse, error)
}

type Cache interface {
	Set(key, value string) error
	Get(key string) (string, error)
}

type Scheduler interface {
	Start()
	ScheduleJob(job models.Job, resultChan chan<- models.APIResponse) error
	StopJob(tag string) error
}

type Webhook interface {
	Invoke(models.WebhookRequest) error
}
