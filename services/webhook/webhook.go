package webhook

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"

	"github.com/hawwwdi/health-check-service/models"
)

type Webhook struct {
	url    string
	method string
	client *http.Client
}

func NewWebhook(url, method string) *Webhook {
	return &Webhook{
		url:    url,
		method: method,
		client: &http.Client{},
	}
}

func (w *Webhook) Invoke(body models.WebhookRequest) error {
	log.Printf("invoking webhook for api %d\n", body.New.Id)
	data, _ := json.Marshal(body)
	request, _ := http.NewRequest(w.method, w.url, bytes.NewBuffer(data))
	_, err := w.client.Do(request)
	if err != nil {
		return err
	}
	return nil
}
