package scheduler

import (
	"time"

	"github.com/go-co-op/gocron"
	"github.com/hawwwdi/health-check-service/models"
)

type scheduler struct {
	engine *gocron.Scheduler
}

func NewScheduler() *scheduler {
	engine := gocron.NewScheduler(time.UTC)
	engine.TagsUnique()
	return &scheduler{engine: engine}
}

func (s *scheduler) Start() {
	s.engine.StartAsync()
}

func (s *scheduler) ScheduleJob(job models.Job, resultChan chan<- models.APIResponse) error {
	_, err := s.engine.Every(job.Interval).Seconds().Tag(job.Tag).Do(job.JobFunc, resultChan)
	if err != nil {
		return err
	}
	return nil
}

func (s *scheduler) StopJob(tag string) error {
	return s.engine.RemoveByTag(tag)
}
