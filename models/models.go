package models

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"
)

type Job struct {
	Tag      string
	Interval int
	JobFunc  func(chan<- APIResponse)
}

type API struct {
	Id       int             `json:"id" db:"id" binding:"-"`
	Url      string          `json:"url" db:"url" binding:"required"`
	Method   string          `json:"method" db:"method" binding:"required"`
	Body     string          `json:"body" db:"body" binding:"required"`
	Headers  json.RawMessage `json:"headers" db:"headers" binding:"-"`
	Interval int             `json:"interval" db:"interval" binding:"required"`
	Started  bool            `json:"started" db:"started" binding:"-"`
}

type APIResponse struct {
	Id        int    `json:"id" db:"id"`
	Status    int    `json:"status" db:"status"`
	Healthy   bool   `json:"healthy" db:"healthy"`
	Error     string `json:"error" db:"error"`
	TimeStamp string `json:"time_stamp" db:"timestamp"`
}

type WebhookRequest struct {
	New  APIResponse `json:"new_response"`
	Last APIResponse `json:"last_response"`
}

func (a API) AsJob() Job {
	var client http.Client
	req, _ := http.NewRequest(a.Method, a.Url, bytes.NewReader([]byte(a.Body)))
	headers := make(map[string]string)
	_ = json.Unmarshal(a.Headers, &headers)
	for k, v := range headers {
		req.Header.Add(k, v)
	}
	return Job{
		Tag:      fmt.Sprintf("%d", a.Id),
		Interval: a.Interval,
		JobFunc: func(result chan<- APIResponse) {
			log.Printf("starting job %v\n", a.Id)
			res := APIResponse{
				Id:        a.Id,
				TimeStamp: time.Now().UTC().Format(time.RFC3339),
			}
			response, err := client.Do(req)
			if err != nil {
				res.Error = err.Error()
				result <- res
				return
			}
			body, _ := ioutil.ReadAll(response.Body)
			err = json.Unmarshal(body, &res)
			if err != nil {
				res.Error = err.Error()
			}
			res.Status = response.StatusCode
			result <- res
		},
	}
}
