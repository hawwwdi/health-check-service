package controllers

import "github.com/hawwwdi/health-check-service/models"

type ApiService interface {
	CreateApi(api models.API) (int, error)
	DeleteApi(id int) error
	StartApi(id int) error
	StopApi(id int) error
	GetAllApi() ([]models.API, error)
}
