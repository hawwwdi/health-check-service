package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/hawwwdi/health-check-service/models"
)

type ApiController struct {
	service ApiService
}

func NewApiController(apiService ApiService) *ApiController {
	return &ApiController{service: apiService}
}

func (a *ApiController) CreateApi(ctx *gin.Context) {
	var dto models.API
	if err := ctx.ShouldBindJSON(&dto); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"err": err.Error(),
		})
		return
	}
	id, err := a.service.CreateApi(dto)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, gin.H{
		"id": id,
	})
}

func (a *ApiController) DeleteApi(ctx *gin.Context) {
	var dto struct {
		Id int `json:"id" binding:"required"`
	}
	if err := ctx.ShouldBindJSON(&dto); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"err": err.Error(),
		})
		return
	}
	err := a.service.DeleteApi(dto.Id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.Status(http.StatusOK)
}

func (a *ApiController) StartApi(ctx *gin.Context) {
	var dto struct {
		Id int `json:"id" binding:"required"`
	}
	if err := ctx.ShouldBindJSON(&dto); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"err": err.Error(),
		})
		return
	}
	err := a.service.StartApi(dto.Id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.Status(http.StatusOK)
}

func (a *ApiController) StopApi(ctx *gin.Context) {
	var dto struct {
		Id int `json:"id" binding:"required"`
	}
	if err := ctx.ShouldBindJSON(&dto); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H{
			"err": err.Error(),
		})
		return
	}
	err := a.service.StopApi(dto.Id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.Status(http.StatusOK)
}

func (a *ApiController) GetAllApi(ctx *gin.Context) {
	res, err := a.service.GetAllApi()
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}
	ctx.JSON(http.StatusOK, res)
}
