package configs

import "github.com/kelseyhightower/envconfig"

type (
	Config struct {
		App
		Database
		Cache
		Webhook
	}
	App struct {
		Port       string `envconfig:"APP_PORT" default:"3000"`
		BufferSize int    `envconfig:"APP_BUFFER_SIZE"`
		Workers    int    `envconfig:"APP_WORKERS"`
	}

	Webhook struct {
		Url    string `envconfig:"WEBHOOK_URL"`
		Method string `envconfig:"WEBHOOK_METHOD"`
	}

	Database struct {
		Host     string `envconfig:"DATABASE_HOST"`
		Port     int    `envconfig:"DATABASE_PORT"`
		User     string `envconfig:"DATABASE_USER"`
		Password string `envconfig:"DATABASE_PASSWORD"`
		DbName   string `envconfig:"DATABASE_DBNAME"`
		Extras   string `envconfig:"DATABASE_EXTRAS"`
		Driver   string `envconfig:"DATABASE_DRIVER" default:"postgres"`
	}

	Cache struct {
		Host     string `envconfig:"CACHE_HOST"`
		Port     string `envconfig:"CACHE_PORT"`
		Password string `envconfig:"CACHE_PASSWORD"`
	}
)

var Configs Config

func Load() error {
	err := envconfig.Process("", &Configs)
	return err
}
