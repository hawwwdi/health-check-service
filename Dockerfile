FROM golang:1.17.12-alpine3.16 AS builder

WORKDIR /app

COPY go.mod .

RUN go mod tidy

COPY . . 

RUN go build -o app-bin . 

FROM alpine

WORKDIR /app

COPY --from=builder /app/app-bin .

EXPOSE 3001

CMD ["./app-bin"]