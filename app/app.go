package app

import (
	"github.com/gin-gonic/gin"
	"github.com/hawwwdi/health-check-service/controllers"
)

type App struct {
	r *gin.Engine
}

func NewApp(c *controllers.ApiController) *App {
	gin.ForceConsoleColor()
	r := gin.Default()
	r.Use(gin.Recovery(), gin.Logger())
	api := r.Group("/api")
	{
		api.POST("/create", c.CreateApi)
		api.POST("/delete", c.DeleteApi)
		api.POST("/start", c.StartApi)
		api.POST("/stop", c.StopApi)
		api.GET("", c.GetAllApi)
	}
	return &App{r: r}
}

func (a *App) Start(addr ...string) error {
	return a.r.Run(addr...)
}
