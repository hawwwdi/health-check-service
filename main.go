package main

import (
	"log"

	"github.com/hawwwdi/health-check-service/app"
	"github.com/hawwwdi/health-check-service/configs"
	"github.com/hawwwdi/health-check-service/controllers"
	"github.com/hawwwdi/health-check-service/db"
	apiservice "github.com/hawwwdi/health-check-service/services/apiService"
	"github.com/hawwwdi/health-check-service/services/scheduler"
	"github.com/hawwwdi/health-check-service/services/webhook"
	apistore "github.com/hawwwdi/health-check-service/store/apiStore"
	"github.com/hawwwdi/health-check-service/store/cache"
	"github.com/joho/godotenv"
)

func init() {
	log.SetFlags(log.Llongfile | log.Ltime)
	err := godotenv.Load()
	if err != nil {
		panic(err)
	}
	err = configs.Load()
	if err != nil {
		panic(err)
	}
}

func main() {
	db, err := db.NewDB(
		configs.Configs.Database.Driver,
		configs.Configs.Database.Host,
		configs.Configs.Database.User,
		configs.Configs.Database.Password,
		configs.Configs.Database.DbName,
		configs.Configs.Database.Extras,
		configs.Configs.Database.Port)
	if err != nil {
		log.Fatal(err)
	}
	apiStore := apistore.NewApisStore(db)
	scheduler := scheduler.NewScheduler()
	//cache := cache.NewMemoryCache()
	rcache, err := cache.NewRedisCache(configs.Configs.Cache.Host+":"+configs.Configs.Cache.Port, configs.Configs.Cache.Password)
	if err != nil {
		log.Println(err)
		log.Println("using in memory cahce")
		rcache = cache.NewMemoryCache()
	}
	webhook := webhook.NewWebhook(configs.Configs.Webhook.Url, configs.Configs.Webhook.Method)
	apiService := apiservice.NewApiService(apiStore, scheduler, webhook, rcache, 10)
	if err = apiService.Start(10); err != nil {
		log.Fatal(err)
	}
	apiController := controllers.NewApiController(apiService)
	app := app.NewApp(apiController)
	app.Start(":" + configs.Configs.App.Port)
}
