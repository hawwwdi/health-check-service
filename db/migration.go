package db

type migration struct {
	name string
	ddl  string
}

var migrations = []migration{
	{
		name: "create_table_api",
		ddl:  createTableApi,
	},
	{
		name: "delete_table_api_health",
		ddl:  createTableApiHealth,
	},
}

var createTableApi = `
create table if not exists api (
    id serial primary key,
    url varchar(255) not null,
    method varchar(16) not null,
    body varchar(255) not null,
    headers json,
    interval int not null,
    started bool not null default false
);`

var createTableApiHealth = `
create table if not exists api_health (
    id int not null references api(id) on delete cascade,
    healthy bool not null,
    status int not null,
    error varchar(255),
    timestamp timestamp not null
);`
