package db

import (
	"errors"
	"fmt"
	"log"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type Driver int

const (
	MySql Driver = iota
	Postgres
)

type DB struct {
	Conn   *sqlx.DB
	Driver Driver
}

var ErrDriverNotSupported = errors.New("driver not supported")

func NewDB(driver, host, user, password, dbName, extras string, port int) (*DB, error) {
	var driverType Driver
	switch driver {
	case "mysql":
		driverType = MySql
	case "postgres":
		driverType = Postgres
	default:
		return nil, ErrDriverNotSupported
	}
	connectionStr := fmt.Sprintf("host=%s port=%d user=%s "+
		"password=%s dbname=%s %s",
		host, port, user, password, dbName, extras)
	conn, err := sqlx.Connect(driver, connectionStr)
	if err != nil {
		return nil, err
	}
	if err := migrate(conn); err != nil {
		return nil, err
	}
	return &DB{Driver: driverType, Conn: conn}, nil
}

func migrate(conn *sqlx.DB) error {
	for _, migration := range migrations {
		log.Printf("execute %s migration\n", migration.name)
		if _, err := conn.Exec(migration.ddl); err != nil {
			return err
		}
	}
	return nil
}
