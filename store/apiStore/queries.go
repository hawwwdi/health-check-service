package apistore

const createApi = `
insert into api(url, method, body, headers, interval)
values ($1, $2, $3, $4, $5)
RETURNING id;`

const deleteApi = `
DELETE FROM api WHERE id = $1;`

const startApi = `
update api
set started = true
WHERE id = $1;`

const stopApi = `
update api
set started = false
WHERE id = $1;
`

const getAllApi = `
select * from api;`

const getApi = `
select * from api where id = $1;`

const submitResp = `
insert into api_health (id, healthy, status, error, timestamp)
values ($1, $2, $3, $4, $5);`

const getResp = `
select *
from api_health
where id = $1
ORDER BY timestamp DESC
LIMIT 1;`
