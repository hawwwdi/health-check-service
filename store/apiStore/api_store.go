package apistore

import (
	"context"

	"github.com/hawwwdi/health-check-service/db"
	"github.com/hawwwdi/health-check-service/models"
)

type apiStore struct {
	db *db.DB
}

func NewApisStore(db *db.DB) *apiStore {
	return &apiStore{db: db}
}

func (a *apiStore) Create(ctx context.Context, api models.API) (int, error) {
	var stmt string
	switch a.db.Driver {
	case db.Postgres:
		stmt = createApi
	default:
		return -1, db.ErrDriverNotSupported
	}
	var id int
	err := a.db.Conn.QueryRowxContext(ctx, stmt, api.Url, api.Method, api.Body, api.Headers, api.Interval).Scan(&id)
	if err != nil {
		return -1, err
	}
	return id, nil
}

func (a *apiStore) Delete(ctx context.Context, id int) error {
	var stmt string
	switch a.db.Driver {
	case db.Postgres:
		stmt = deleteApi
	default:
		return db.ErrDriverNotSupported
	}
	_, err := a.db.Conn.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}
	return nil
}

func (a *apiStore) Start(ctx context.Context, id int) error {
	var stmt string
	switch a.db.Driver {
	case db.Postgres:
		stmt = startApi
	default:
		return db.ErrDriverNotSupported
	}
	_, err := a.db.Conn.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}
	return nil
}

func (a *apiStore) Stop(ctx context.Context, id int) error {
	var stmt string
	switch a.db.Driver {
	case db.Postgres:
		stmt = stopApi
	default:
		return db.ErrDriverNotSupported
	}
	_, err := a.db.Conn.ExecContext(ctx, stmt, id)
	if err != nil {
		return err
	}
	return nil
}

func (a *apiStore) GetAll(ctx context.Context) ([]models.API, error) {
	var stmt string
	switch a.db.Driver {
	case db.Postgres:
		stmt = getAllApi
	default:
		return nil, db.ErrDriverNotSupported
	}
	rows, err := a.db.Conn.QueryxContext(ctx, stmt)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	res := make([]models.API, 0)
	for rows.Next() {
		var curr models.API
		err := rows.StructScan(&curr)
		if err != nil {
			return nil, err
		}
		res = append(res, curr)
	}
	return res, nil
}

func (a *apiStore) Get(ctx context.Context, id int) (models.API, error) {
	var stmt string
	switch a.db.Driver {
	case db.Postgres:
		stmt = getApi
	default:
		return models.API{}, db.ErrDriverNotSupported
	}
	var api models.API
	err := a.db.Conn.QueryRowxContext(ctx, stmt, id).StructScan(&api)
	if err != nil {
		return models.API{}, err
	}
	return api, nil
}

func (a *apiStore) SubmitResponse(ctx context.Context, resp models.APIResponse) error {
	var stmt string
	switch a.db.Driver {
	case db.Postgres:
		stmt = submitResp
	default:
		return db.ErrDriverNotSupported
	}
	_, err := a.db.Conn.ExecContext(ctx, stmt, resp.Id, resp.Healthy, resp.Status, resp.Error, resp.TimeStamp)
	if err != nil {
		return err
	}
	return nil
}

func (a *apiStore) GetLastResponse(ctx context.Context, id int) (models.APIResponse, error) {
	var stmt string
	switch a.db.Driver {
	case db.Postgres:
		stmt = getResp
	default:
		return models.APIResponse{}, db.ErrDriverNotSupported
	}
	var resp models.APIResponse
	err := a.db.Conn.QueryRowxContext(ctx, stmt, id).StructScan(&resp)
	if err != nil {
		return models.APIResponse{}, err
	}
	return resp, nil
}
