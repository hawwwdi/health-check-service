package cache

import (
	"context"

	"github.com/go-redis/redis/v8"
)

var noContext = context.Background()

type redisCache struct {
	rdb *redis.Client
}

func NewRedisCache(addr, pass string) (Cache, error) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: pass,
	})
	_, err := rdb.Ping(context.Background()).Result()
	if err != nil {
		return nil, err
	}
	return &redisCache{
		rdb: rdb,
	}, nil
}

func (r *redisCache) Set(key, value string) error {
	return r.rdb.Set(noContext, key, value, 0).Err()
}

func (r *redisCache) Get(key string) (string, error) {
	key, err := r.rdb.Get(noContext, key).Result()
	if err != nil {
		return "", ErrKeyNotFound
	}
	return key, nil
}
