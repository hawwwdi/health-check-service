package cache

import "errors"

var ErrKeyNotFound = errors.New("key not found")

type Cache interface {
	Set(key, value string) error
	Get(key string) (string, error)
}
