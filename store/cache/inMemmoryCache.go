package cache

import "sync"

type MemoryCache struct {
	lock  *sync.RWMutex
	cache map[string]string
}

func NewMemoryCache() Cache {
	return &MemoryCache{
		cache: make(map[string]string),
		lock:  &sync.RWMutex{},
	}
}

func (m *MemoryCache) Set(key, value string) error {
	m.lock.Lock()
	defer m.lock.Unlock()
	m.cache[key] = value
	return nil
}

func (m *MemoryCache) Get(key string) (string, error) {
	m.lock.RLock()
	defer m.lock.RUnlock()
	value, ok := m.cache[key]
	if !ok {
		return "", ErrKeyNotFound
	}
	return value, nil
}
